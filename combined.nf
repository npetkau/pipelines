nextflow.enable.dsl = 2

include {sradownload} from "./pipeline"
include {assembly_p} from "./assembly"
//include {multiqc} from "./pipeline"

workflow {
    sra = sradownload(params.accession, params.outdir, params.with_fastqc, params.with_stats, params.with_fastp, params.with_multiqc)
    assem = assembly_p(sra.fastq, params.outdir, params.with_valvet, params.with_spades, params.with_quast)
    
}



