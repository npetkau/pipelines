nextflow.enable.dsl = 2
  
//  container "https://depot.galaxyproject.org/singularity/"

params.with_fastqc = false
params.with_stats = false
params.with_fastp = false
params.with_multiqc= false

process prefetch {
  storeDir "${params.sradownload_outdir}"
  container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
  input: 
    val accession
  output:
    path "${accession}/${accession}.sra"
  script:
    """
    prefetch $accession
    """
}

process fastqDump {
    publishDir "${params.sradownload_outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/sra-tools:2.11.0--pl5262h314213e_0"
    input:
        path sraresult
    output:
        path "*.fastq", emit: fastq
    script:
    """
    fastq-dump --split-e ${sraresult} 
    """
}

process stats {
  publishDir "${params.sradownload_outdir}/stats", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/ngsutils:0.5.9--py27heb79e2c_4"
  input:
    path stats_channel
  output:
    path "${stats_channel.getSimpleName()}.txt"
  script:
    """
    fastqutils stats ${stats_channel}> ${stats_channel.getSimpleName()}.txt

    """
}


process fastp {
  publishDir "${params.sradownload_outdir}/fastp/${fastp_ch.getSimpleName()}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastp:0.22.0--h2e03b76_0"
  input:
    path fastp_ch
  output:
    path "${fastp_ch.getSimpleName()}_fastp.fastq", emit: fastp_fastq
    path "*_fastp.json", emit: fastp_report
  script:
    """
    fastp -i ${fastp_ch} -o ${fastp_ch.getSimpleName()}_fastp.fastq -j ${fastp_ch.getSimpleName()}_fastp.json

    """
}

process fastqc {
  publishDir "${params.sradownload_outdir}/fastqc/${quality.getSimpleName()}", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--hdfd78af_1"
  input:
    path quality
  output:
    path "*fastqc.zip", emit: zip
    path "*fastqc.html", emit: html
    // path "${quality.getSimpleName()}_fastqc.*" // alternative
    // "fastqc_results/*" // alternative_script
  script:
    """
    fastqc ${quality}
    # mkdir fastqc_results # alternative_script
    # fastqc -o ./fastqc_results ${quality}

    """
}

process multiqc {
  publishDir "${params.sradownload_outdir}/multiqc", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/multiqc:1.9--pyh9f0ad1d_0"
  input:
    path fastqc_fastp
  output:
    path "*.html"
    path "./multiqc_data"
  script:
    """
    multiqc ${fastqc_fastp}

    """
}

workflow sradownload {
  take:
    accession
    outdir
    with_fastqc
    with_stats
    with_fastp
    with_multiqc
  main:
    params.sradownload_outdir = outdir
    sraresult = prefetch(accession)
    fastqDump_channel = fastqDump(sraresult)
    fastp_fastq_channel = channel.empty()
    
    if(with_stats) {
    stats_channel = stats(fastqDump_channel.flatten())
    }
  if(with_fastp) {
    fastp_channel = fastp(fastqDump_channel.flatten())
    fastp_fastq_channel = fastp_channel.fastp_fastq
    }
  if(with_fastqc) {
    fastqc_channel = fastqc(fastp_fastq_channel.flatten()
.concat(fastqDump_channel).flatten())
    }

  if(with_multiqc) {
    multiqc_channel = multiqc(fastp_channel.fastp_report.collect().concat(fastqc_channel.zip).collect())
    }
    
  emit:
    fastq = fastp_channel.fastp_fastq.collect()
}





workflow {
  download = sradownload(params.accession, params.outdir, params.with_fastqc, params.with_stats, params.with_fastp, params.with_multiqc)
}

// singularity exec docker://ncbi/sra-tools fastq-dump --split-files SRR1777174





