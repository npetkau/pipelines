nextflow.enable.dsl = 2

params.kmerlen = 71
params.cutoff = 0
params.length_cutoff = 0

params.with_valvet = false
params.with_spades = false
params.with_quast = false
//params.with_multiqc = false
//params.reference = null

process valvet {
    publishDir "${params.assembly_outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/velvet:1.2.10--h5bf99c6_4"
    input:
        path fastqfiles
    output:
        path "${fastqfiles.getSimpleName()}_valvet.fasta", emit: velvet
    script:
     if(fastqfiles instanceof List) {
      """
      velveth valvet/ ${params.kmerlen} -fastq -shortPaired ${fastqfiles}
      velvetg valvet/ -cov_cutoff ${params.cutoff} -min_contig_lgth ${params.length cutoff}
      """
    } else {
      """
      velveth valvet/ ${params.kmerlen} -fastq -short ${fastqfiles}
      
      velvetg valvet/ -cov_cutoff ${params.cutoff} -min_contig_lgth ${params.length_cutoff}
      cp ./valvet/*.fa ./${fastqfiles.getSimpleName()}_valvet.fasta
      """
    }
}

process spades {
    publishDir "${params.assembly_outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/spades:3.15.3--h95f258a_0"
    input:
        path fastqfiles
    output:
        path "${fastqfiles.getSimpleName()}_spades.fasta", emit: spades
    script:
      """
      spades.py -s ${fastqfiles} -o spades/
      cp ./spades/contigs.fasta ./${fastqfiles.getSimpleName()}_spades.fasta
      """
}

process quast {
    publishDir "${params.assembly_outdir}", mode: "copy" , overwrite: true
    container "https://depot.galaxyproject.org/singularity/quast:5.0.2--py37pl5262h190e900_4"
    input:
        path infile
//        path reference
    output:
        path "*"
    script:
      """
      quast.py ${infile} -o quast/
      
      """
}
/*
process multiqc {
  publishDir "${params.outdir}/multiqc", mode: "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/multiqc:1.9--pyh9f0ad1d_0"
  input:
    path infile
  output:
    path "*.html"
    path "./multiqc_data"
  script:
    """
    multiqc ${infile}

    """
}
*/

workflow assembly_p {
  take:
    fastqfiles_ch
    outdir
    with_valvet
    with_spades
    with_quast

  main:
    params.assembly_outdir = outdir
    //fastqfiles_ch = channel.fromPath("${params.outdir}/*.fastq").collect()

  if(params.with_valvet) {
    valvet_ch = valvet(fastqfiles_ch)
  }
  if(params.with_spades) {
    spades_ch = spades(fastqfiles_ch)
  }
  if(params.with_quast) {
    quast_ch = quast(valvet_ch.concat(spades_ch).collect())
  }
}
workflow {
  fastqfiles_ch = channel.fromPath("${params.outdir}/*.fastq").collect()
  assembly(fastqfiles_ch,params.outdir, params.with_valvet, params.with_spades, params.with_quast)
}



/*
workflow {
  fastqfiles_ch = channel.fromPath("${params.outdir}/*.fastq").collect()

  if(params.with_valvet) {
    valvet_ch = valvet(fastqfiles_ch)
}
  if(params.with_spades) {
    spades_ch = spades(fastqfiles_ch)
}

//  reference_ch = channel.empty()
//  if(params.reference != null) {
//    reference_ch = channel.fromPath("${params.reference}")
//}

  if(params.with_quast) {
    quast_ch = quast(valvet_ch.concat(spades_ch).collect())
    //quast_ch = quast(valvet_ch.concat(spades_ch).collect(),reference_ch)
}
//  if(params.with_multiqc) {
//    multiqc_channel = multiqc(quast_ch)
//}
}
*/
